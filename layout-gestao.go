package layout_gestao

import (
	"bitbucket.org/infarma/layout-gestao/arquivoDePedido"
	arquivoDePedido31 "bitbucket.org/infarma/layout-gestao/v_3_1/arquivoDePedido"
	arquivoDePedido40 "bitbucket.org/infarma/layout-gestao/v_4_0/arquivoDePedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoDePedido.ArquivoDePedido, error) {
	return arquivoDePedido.GetStruct(fileHandle)
}

func GetArquivoDePedido31(fileHandle *os.File) (arquivoDePedido31.ArquivoDePedido, error) {
	return arquivoDePedido31.GetStruct(fileHandle)
}

func GetArquivoDePedido40(fileHandle *os.File) (arquivoDePedido40.ArquivoDePedido, error) {
	return arquivoDePedido40.GetStruct(fileHandle)
}
