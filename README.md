### GESTAO
## Arquivo de Pedido
gerador-layouts arquivoDePedido header NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoCliente:string:6:20 DataGeracao:int32:20:28 HoraGeracao:int32:28:32 NrPedido:int32:32:38 PedidoConsulta:int32:38:39 FormaPagamento:string:39:43

gerador-layouts arquivoDePedido produto NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoProduto:int64:6:19 Fixo:int64:19:32 Quantidade:int32:32:36 Filler:int32:36:40

gerador-layouts arquivoDePedido trailler NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 QuantidadeTipo2:int32:6:10 Filler:int32:10:40


## Arquivo de Retorno
gerador-layouts retornoDePedido header NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoCliente:string:6:20 DataGeracao:int32:20:28 HoraGeracao:int32:28:32 NrPedido:int32:32:38 PedidoConsulta:int32:38:39 NrPedidoFornecedor:string:39:49

gerador-layouts retornoDePedido produto NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoProduto:int64:6:19 ValorUnitario:float64:19:32:2 Quantidade:int32:32:36 PercentualDesconto:float64:36:45:2

gerador-layouts retornoDePedido trailler NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 Fixo:string:6:16 ValorBruto:float64:16:26:2 ValorDesconto:float64:26:36:2 Filler:int32:36:40



### GESTAO31
## Aquivo de Pedido
gerador-layouts arquivoDePedido header NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoiCliente:string:6:20 DataGeracao:int32:20:28 HoraGeracao:int32:28:32 NrPedido:int32:32:38 PedidoConsulta:int32:38:39 FormaPagamento:int64:39:48 CodigoIternoFilial:string:48:53

gerador-layouts arquivoDePedido produto NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoiProduto:int64:6:19 Fixo:string:19:32 Quantidade:int32:32:36 Filler:string:36:40

gerador-layouts arquivoDePedido trailler NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 QuantidadeRegistroTipo2:int32:6:10 Filler:string:10:40


## Arquivo de Retorno
gerador-layouts retornoDePedido header NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoiCliente:string:6:20 DataGeracao:int32:20:28 HoraGeracao:int32:28:32 NrPedido:int32:32:38 PedidoConsulta:int32:38:39 NrPedidoFornecedor:int64:39:49

gerador-layouts retornoDePedido produto NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoiProduto:int64:6:19 ValorUnitario:float64:19:32:2 Quantidade:int32:32:36 PercentualDesconto:float64:36:45:2 CodigoMensagemRetorno:string:45:51

gerador-layouts retornoDePedido trailler NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 Fixo:string:6:16 ValorBruto:float64:16:26:2 ValorDesconto:float64:26:36:2 Filler:string:36:40



### GESTAO
## Arquivo de Pedido
gerador-layouts arquivoDePedido40 header NumeroSequencial:int32:0:7 TipoRegistro:string:7:9 CodigoCliente:string:9:23 DataGeracao:int32:23:31 HoraGeracao:int32:31:35 NrPedido:int32:35:49 PedidoConsulta:int32:49:50 FormaPagamento:string:50:53

gerador-layouts arquivoDePedido40 produto NumeroSequencial:int32:0:7 TipoRegistro:string:7:9 CodigoProduto:int64:9:22 Fixo:int64:22:35 Quantidade:int32:35:40 Filler:int32:40:44

gerador-layouts arquivoDePedido40 trailler NumeroSequencial:int32:0:7 TipoRegistro:string:7:9 QuantidadeTipo2:int32:9:21 Filler:int32:21:25


## Arquivo de Retorno
gerador-layouts retornoDePedido40 header NumeroSequencial:int32:0:7 TipoRegistro:string:7:9 CodigoCliente:string:9:23 DataGeracao:int32:23:31 HoraGeracao:int32:31:35 NrPedido:int32:35:49 PedidoConsulta:int32:49:50 NrPedidoFornecedor:string:50:64

gerador-layouts retornoDePedido40 produto NumeroSequencial:int32:0:7 TipoRegistro:string:7:9 CodigoProduto:int64:9:22 ValorUnitario:float64:22:35:2 Quantidade:int32:35:39 PercentualDesconto:float64:39:48:2

gerador-layouts retornoDePedido trailler NumeroSequencial:int32:0:7 TipoRegistro:string:7:9 Fixo:string:9:19 ValorBruto:float64:19:29:2 ValorDesconto:float64:29:39:2 Filler:int32:39:43