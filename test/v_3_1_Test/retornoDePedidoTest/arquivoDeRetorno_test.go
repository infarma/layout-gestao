package retornoDePedidoTest

import (
	"fmt"
	"layout-gestao/v_3_1/retornoDePedido"
	"testing"
	"time"
)

func TestHeaderRetorno (t *testing.T) {

	data := time.Date(2019, 10,1,1,5,20,0, time.UTC)

	cabecalho := retornoDePedido.GetHeaderRetorno(2222, "33333333333333",data,data,66666666666666,1,99999999999999 )

	if cabecalho != "2222013333333333333301102019010566666666666666199999999999999" {
		fmt.Println("cabecalho", cabecalho)
		t.Error("cabecalho não é compativel")

	}

}

func TestProdutoRetorno (t *testing.T) {

	Valor := float64(56.35)
	Produto := retornoDePedido.GetProdutoRetorno(3333,"4444444444444", Valor, 1234, Valor, "666666")

	if Produto != "333302444444444444400000000056351234000005635666666" {
		fmt.Println("Produto", Produto)
		t.Error("Produto não é compativel")

	}

}

func TestTrailerRetorno (t *testing.T) {

	Valor := float64(56.35)
	Trailer := retornoDePedido.GetTrailerRetorno(5555,Valor,Valor)

	if Trailer != "5555030000000000000000563500000056350000" {
		fmt.Println("Trailer", Trailer)
		t.Error("Trailer não é compativel")

	}

}
