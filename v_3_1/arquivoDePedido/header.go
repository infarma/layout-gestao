package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	NumeroSequencial   int32 	`json:"NumeroSequencial"`
	TipoRegistro       string	`json:"TipoRegistro"`
	CodigoCliente     string	`json:"CodigoCliente"`
	DataGeracao        int32 	`json:"DataGeracao"`
	HoraGeracao        int32 	`json:"HoraGeracao"`
	NrPedido           int32 	`json:"NrPedido"`
	PedidoConsulta     int32 	`json:"PedidoConsulta"`
	FormaPagamento     int64 	`json:"FormaPagamento"`
	CodigoIternoFilial string	`json:"CodigoIternoFilial"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoCliente, "CodigoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataGeracao, "DataGeracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.HoraGeracao, "HoraGeracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NrPedido, "NrPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.PedidoConsulta, "PedidoConsulta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.FormaPagamento, "FormaPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoIternoFilial, "CodigoIternoFilial")
	if err != nil {
		return err
	}


	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial":                      {0, 4, 0},
	"TipoRegistro":                      {4, 6, 0},
	"CodigoCliente":                      {6, 20, 0},
	"DataGeracao":                      {20, 28, 0},
	"HoraGeracao":                      {28, 32, 0},
	"NrPedido":                      {32, 38, 0},
	"PedidoConsulta":                      {38, 39, 0},
	"FormaPagamento":                      {39, 48, 0},
	"CodigoIternoFilial":                      {48, 53, 0},
}