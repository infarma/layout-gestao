package retornoDePedido

import (
	"fmt"
	"strings"
	"time"
)

func GetHeaderRetorno(	NumeroSequencial int64,  CodigoCliente string, DataGeracao time.Time, HoraGeracao time.Time, NrPedido int64, PedidoConsulta int32, NrPedidoFornecedor int64 ) string{


	cabecalho := fmt.Sprintf("%04d", NumeroSequencial)
	//tipo de registro
	cabecalho += "01"
	cabecalho += fmt.Sprintf("%014s", CodigoCliente)
	cabecalho += DataGeracao.Format("02012006")
	cabecalho += HoraGeracao.Format("1504")
	cabecalho += fmt.Sprintf("%06d", NrPedido )
	cabecalho += fmt.Sprintf("%01d", PedidoConsulta )
	cabecalho += fmt.Sprintf("%010d", NrPedidoFornecedor )

	return cabecalho
}

func GetProdutoRetorno(	NumeroSequencial int64,  CodigoProduto string, ValorUnitario float64, Quantidade int32, PercentualDesconto float64, CodigoMensagemRetorno string ) string{


	Produto := fmt.Sprintf("%04d", NumeroSequencial)
	//tipo de registro
	Produto += "02"
	Produto += fmt.Sprintf("%013s", CodigoProduto)
	Produto += fmt.Sprintf("%013s", strings.Replace(fmt.Sprintf("%.2f", ValorUnitario), ".", "", 1))
	Produto += fmt.Sprintf("%04d", Quantidade )
	Produto += fmt.Sprintf("%09s", strings.Replace(fmt.Sprintf("%.2f", PercentualDesconto), ".", "", 1))
	Produto += fmt.Sprintf("%06s", CodigoMensagemRetorno )

	return Produto
}

func GetTrailerRetorno(	NumeroSequencial int64, ValorBruto float64, ValorDesconto float64 ) string{


	Trailer := fmt.Sprintf("%04d", NumeroSequencial)
	//tipo de registro
	Trailer += "03"
	//fixo
	Trailer += fmt.Sprintf("%010s", "")
	Trailer += fmt.Sprintf("%010s", strings.Replace(fmt.Sprintf("%.2f", ValorBruto), ".", "", 1))
	Trailer += fmt.Sprintf("%010s", strings.Replace(fmt.Sprintf("%.2f", ValorDesconto), ".", "", 1))
	//filler
	Trailer += fmt.Sprintf("%04s", "")

	return Trailer
}