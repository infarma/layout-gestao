package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Produto struct {
	NumeroSequencial      int32  	`json:"NumeroSequencial"`
	TipoRegistro          string 	`json:"TipoRegistro"`
	CodigoProduto        int64  	`json:"CodigoProduto"`
	ValorUnitario         float64	`json:"ValorUnitario"`
	Quantidade            int32  	`json:"Quantidade"`
	PercentualDesconto    float64	`json:"PercentualDesconto"`
	CodigoMensagemRetorno string 	`json:"CodigoMensagemRetorno"`
}

func (p *Produto) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesProduto

	err = posicaoParaValor.ReturnByType(&p.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorUnitario, "ValorUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PercentualDesconto, "PercentualDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoMensagemRetorno, "CodigoMensagemRetorno")
	if err != nil {
		return err
	}


	return err
}

var PosicoesProduto = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial":                      {0, 4, 0},
	"TipoRegistro":                      {4, 6, 0},
	"CodigoProduto":                      {6, 19, 0},
	"ValorUnitario":                      {19, 32, 2},
	"Quantidade":                      {32, 36, 0},
	"PercentualDesconto":                      {36, 45, 2},
	"CodigoMensagemRetorno":                      {45, 51, 0},
}