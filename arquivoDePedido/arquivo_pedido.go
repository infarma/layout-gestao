package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Header   Header    `json:"Header"`
	Produto  []Produto `json:"Produto"`
	Trailler Trailler  `json:"Trailler"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[4:6])

		if identificador == "01" {
			err = arquivo.Header.ComposeStruct(string(runes))
		} else if identificador == "02" {
			Produto := Produto{}
			Produto.ComposeStruct(string(runes))
			arquivo.Produto = append(arquivo.Produto,Produto)
		} else if identificador == "03" {
			err = arquivo.Trailler.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
