package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailler struct {
	NumeroSequencial int32  `json:"NumeroSequencial"`
	TipoRegistro     string `json:"TipoRegistro"`
	QuantidadeTipo2  int32  `json:"QuantidadeTipo2"`
	Filler           int32  `json:"Filler"`
}

func (t *Trailler) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailler

	err = posicaoParaValor.ReturnByType(&t.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeTipo2, "QuantidadeTipo2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailler = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial": {0, 4, 0},
	"TipoRegistro":     {4, 6, 0},
	"QuantidadeTipo2":  {6, 10, 0},
	"Filler":           {10, 40, 0},
}
