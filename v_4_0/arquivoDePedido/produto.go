package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Produto struct {
	NumeroSequencial int32 	`json:"NumeroSequencial"`
	TipoRegistro     string	`json:"TipoRegistro"`
	CodigoProduto    int64 	`json:"CodigoProduto"`
	Fixo             int64 	`json:"Fixo"`
	Quantidade       int32 	`json:"Quantidade"`
	Filler           int32 	`json:"Filler"`
}

func (p *Produto) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesProduto

	err = posicaoParaValor.ReturnByType(&p.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Fixo, "Fixo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Filler, "Filler")
	if err != nil {
		return err
	}


	return err
}

var PosicoesProduto = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial":                      {0, 7, 0},
	"TipoRegistro":                      {7, 9, 0},
	"CodigoProduto":                      {9, 22, 0},
	"Fixo":                      {22, 35, 0},
	"Quantidade":                      {35, 40, 0},
	"Filler":                      {40, 44, 0},
}